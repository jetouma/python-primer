# Python Primer
This is the first of a series of Python tutorials that span from the novice to the advanced levels. 

# Notebooks
1. [Basics](../notebooks/01-basics.ipynb)
2. [Operators](../notebooks/02-operators.ipynb)
3. [Basic Formatting](../notebooks/03-basic_formatting.ipynb)
4. [If statement](../notebooks/04-if_statement.ipynb)
5. [Looping](../notebooks/05-looping.ipynb)
6. [Lists](../notebooks/06-lists.ipynb)
7. [Dictionaries](../notebooks/07-dictionaries.ipynb)
8. [Tuples](../notebooks/08-tuples.ipynb)
9. [Sets](../notebooks/09-sets.ipynb)
10. [Strings](../notebooks/10-strings.ipynb)
11. [Fucntions](../notebooks/11-funtion.ipynb)
12. [Modules](../notebooks/12-modules.ipynb)
13. [Symbolic Math](../notebooks/13-symbolic.ipynb)
14. [OOP](../notebooks/14-oop.ipynb)
15. [YAML-CSV-JSON-XML](../notebooks/15-xml-yaml-json.ipynb)
16. [File System](../notebooks/16-filesystem.ipynb)
